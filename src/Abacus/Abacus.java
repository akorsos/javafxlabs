package Abacus;

import javafx.animation.TranslateTransition;
import javafx.animation.TranslateTransitionBuilder;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CircleBuilder;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import static javafx.util.Duration.millis;

public class Abacus extends Application {

    int rows = 10;
    int columns = 9;
    int radius = 20;
    int diameter = 2 * radius;
    int move = 8 * diameter;
    int width = columns * diameter + move;
    int height = rows * diameter;
    int pad = 20;
    int offset = pad + radius;
    int slider_height = 5;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Abacus");
        Pane root = new Pane();

        for (int row = 0; row < rows; row++) {
            Rectangle rail = RectangleBuilder.create()
                .width(width)
                .height(slider_height)
                .x(pad)
                .y(offset - (slider_height / 2) + (row * diameter))
                .styleClass("rail").build();
            root.getChildren().add(rail);
           
            LinearGradient gradient2 = new LinearGradient(0, 0, 0, 0.5,  true, CycleMethod.REFLECT, new Stop[] {
            new Stop(0, Color.BLACK),
            new Stop(0.1, Color.BLACK),
            new Stop(1, Color.WHITE)
        });
            
            rail.setFill(gradient2);
            
            Circle last = null;
            for (int column = 0; column < columns; column++) {
                final Circle circle = makeCircle(offset + (row * diameter), offset + (column * diameter));
                circle.getStyleClass().add(column < columns / 2 ? "left" : "right");
                root.getChildren().add(circle);

                Text text = new Text(circle.getCenterX()-3, circle.getCenterY()+4, ""+ ((columns - column) % 10));
                text.translateXProperty().bind(circle.translateXProperty());
                text.setOnMouseClicked(circle.getOnMouseClicked());
                text.getStyleClass().add("text");
                root.getChildren().add(text);

                if (last != null) {
                    last.translateXProperty().addListener(new ChangeListener<Number>() {
                        @Override
                        public void changed(ObservableValue<? extends Number> observableValue, Number oldX, Number newX) {
                            if ((Double) newX > circle.getTranslateX()) circle.setTranslateX((Double) newX);
                        }
                    });
                    final Circle finalLast = last;
                    circle.translateXProperty().addListener(new ChangeListener<Number>() {
                        @Override
                        public void changed(ObservableValue<? extends Number> observableValue, Number oldX, Number newX) {
                            if ((Double) newX < finalLast.getTranslateX()) finalLast.setTranslateX((Double) newX);
                        }
                    });
                }
                last = circle;
            }
        }
        primaryStage.setScene(new Scene(root, width + 2 * pad, height + 2 * pad));
        primaryStage.show();
    }

    private Circle makeCircle(int y, int x) {
        final Circle ball = CircleBuilder.create().radius(radius-1).centerX(x).centerY(y).build();
        
        RadialGradient gradient1 = new RadialGradient(0, 0, 0.5, 0.5, 1, true, CycleMethod.NO_CYCLE, new Stop[] {
            new Stop(0, Color.WHITE),
            new Stop(1, Color.BLACK)
        });
        
        ball.setFill(gradient1);
        
        ball.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                double newX = move;
                if (ball.getTranslateX() > 1) {
                    newX = 0;
                }
                TranslateTransition move = TranslateTransitionBuilder.create().node(ball).toX(newX).duration(millis(200)).build();
                move.playFromStart();
            }
        });
        return ball;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
